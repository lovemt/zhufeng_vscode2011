import { commands, window, ViewColumn, workspace } from "vscode";
import Provider from "./Provider";

const Fs = require("fs");
const Open = require("open");
const Path = require("path");
// 提供数据的类
const provider = new Provider();
// 数据注册
window.registerTreeDataProvider("novel-list", provider);

export const webview = commands.registerCommand("openSelectedNovel", (args) => {
  // 利用node api拿到文件
  let result = Fs.readFileSync(args.path, "utf-8");
  const panel = window.createWebviewPanel(
    "novelReadWebview",
    args.name,
    ViewColumn.One,
    {
      enableScripts: true,
      retainContextWhenHidden: true,
    }
  );

  // 用pre标签外加部分css使保持原有样式
  panel.webview.html = `<html>
    <body>
      <pre style="font-size: 14px;flex: 1 1 auto;white-space: pre-wrap;word-wrap: break-word;">
        ${result}
      <pre>
    </body>
  </html>`;
});

export const selectLocalMenu = commands.registerCommand(
  "vs-zjplugin.selectLocalMenu",
  () => {}
);
