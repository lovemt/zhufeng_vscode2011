import { Novel } from "./model";
import { workspace } from "vscode";
const Fs = require("fs");
const Path = require("path");

const LocalNovelsPath = "/Users/zhujunwei/Desktop";

export function getLocalBooks(): Promise<Novel[]> {
  let { location } = workspace.getConfiguration("fileDir");
  console.log(location);
  const files = Fs.readdirSync(location || LocalNovelsPath);
  const loaclnovellist = [] as any;
  files.forEach((file: string) => {
    const extname = Path.extname(file).substr(1);
    if (extname === "txt") {
      const name = Path.basename(file, ".txt");
      const path = Path.join(location || LocalNovelsPath, file);
      loaclnovellist.push({
        path,
        name,
      });
    }
  });
  return Promise.resolve(loaclnovellist);
}
