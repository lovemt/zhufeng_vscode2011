import { TreeItem } from "vscode";
import { Novel } from "./model";

const path = require("path");

function getExtname() {}

export default class NovelTreeItem extends TreeItem {
  info: Novel;

  constructor(info: Novel) {
    super(`${info.name}`);
    console.log(info);
    const extname = path.extname(info.path);
    const tips = [`名称: ${info.name}${extname}`];

    this.info = info;
    // tooltip是悬浮的条提示
    this.tooltip = tips.join("\r\n");
    // 我们设置一下点击该行的命令，并且传参进去
    this.command = {
      command: "openSelectedNovel",
      title: "打开该小说",
      arguments: [{ name: info.name, path: info.path }],
    };
  }
}
