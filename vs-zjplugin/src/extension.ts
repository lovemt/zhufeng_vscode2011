import { ExtensionContext, window, commands } from "vscode";
import { insertLog, timeLog, clearLog } from "./logger";
import { webview } from "./reader/webview";

export function activate(context: ExtensionContext) {
  context.subscriptions.push(insertLog);
  context.subscriptions.push(timeLog);
  context.subscriptions.push(clearLog);
  context.subscriptions.push(webview);
}

// this method is called when your extension is deactivated
export function deactivate() {}
