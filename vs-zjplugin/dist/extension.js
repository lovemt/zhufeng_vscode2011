module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
const logger_1 = __webpack_require__(1);
const webview_1 = __webpack_require__(3);
function activate(context) {
    context.subscriptions.push(logger_1.insertLog);
    context.subscriptions.push(logger_1.timeLog);
    context.subscriptions.push(logger_1.clearLog);
    context.subscriptions.push(webview_1.webview);
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.clearLog = exports.timeLog = exports.insertLog = void 0;
const vscode_1 = __webpack_require__(2);
function insertText(text) {
    const editor = vscode_1.window.activeTextEditor;
    const selection = editor === null || editor === void 0 ? void 0 : editor.selection;
    // 获取光标当前行
    const lineOfSelectedVar = (selection === null || selection === void 0 ? void 0 : selection.active.line) || 0;
    // edit方法获取editBuilder实例，在后一行添加
    editor === null || editor === void 0 ? void 0 : editor.edit((editBuilder) => {
        editBuilder.insert(new vscode_1.Position(lineOfSelectedVar + 1, 0), text);
        vscode_1.commands.executeCommand("editor.action.formatDocument");
    });
}
/**
 * registerCommand完整的 API 是：
 * 	 registerCommand(command: string, callback: (args: any[]) => any, thisArg?: any): Disposable
 *   主要功能是给功能代码(callback)注册一个命令(command)，
 *   然后通过 subscriptions.push() 给插件订阅对应的 command 事件。
 */
exports.insertLog = vscode_1.commands.registerCommand("vs-zjplugin.insertLog", () => {
    //拿到当前编辑页面的内容对象
    const editor = vscode_1.window.activeTextEditor;
    // 拿到光标选中的文本并格式化
    const selection = editor === null || editor === void 0 ? void 0 : editor.selection;
    let text = (editor === null || editor === void 0 ? void 0 : editor.document.getText(selection)) || "";
    text = text.replace(/\‘|\“|\”|\’/gi, "");
    //这里拼出console语句
    let logToInsert = `\n\n`;
    if (/err|error/.test(text)) {
        logToInsert = `\n\n`;
    }
    else if (/\[.*\]/.test(text)) {
        logToInsert = `\nconsole.table(${text});\n`;
    }
    text ? insertText(logToInsert) : insertText("");
});
//添加 time console
function addTimeLog() {
    const editor = vscode_1.window.activeTextEditor;
    const selection = editor === null || editor === void 0 ? void 0 : editor.selection;
    // 获取光标当前行
    const lineOfSelectedVar = (selection === null || selection === void 0 ? void 0 : selection.active.line) || 0;
    const lineEnd = (selection === null || selection === void 0 ? void 0 : selection.end.line) || 0;
    const startTime = `\n`;
    const endTime = `\n`;
    // edit方法获取editBuilder实例，在后一行添加
    editor === null || editor === void 0 ? void 0 : editor.edit((editBuilder) => {
        // editBuilder.insert(new Position(lineOfSelectedVar, 0),"\n")
        editBuilder.insert(new vscode_1.Position(lineOfSelectedVar, 0), startTime);
        editBuilder.insert(new vscode_1.Position(lineEnd + 1, 0), "\n");
        editBuilder.insert(new vscode_1.Position(lineEnd + 1, 0), endTime);
    });
}
//插入console.time
exports.timeLog = vscode_1.commands.registerCommand("vs-zjplugin.timeLog", () => {
    //拿到当前编辑页面的内容对象
    const editor = vscode_1.window.activeTextEditor;
    // 拿到光标选中的文本并格式化
    const selection = editor === null || editor === void 0 ? void 0 : editor.selection;
    const text = (editor === null || editor === void 0 ? void 0 : editor.document.getText(selection)) || "";
    text ? addTimeLog() : void 0;
});
function getAllLogStatements() {
    const editor = vscode_1.window.activeTextEditor;
    // 获取编辑器页面文本
    const document = editor.document;
    const documentText = (document === null || document === void 0 ? void 0 : document.getText()) || "";
    let logStatements = [];
    // 检测console的正则表达式
    const logRegex = /console.(log|debug|info|warn|error|assert|dir|dirxml|trace|group|groupEnd|time|timeEnd|profile|profileEnd|count)\((.*)\);?/g;
    let match;
    // 正则循环匹配页面文本
    while ((match = logRegex.exec(documentText))) {
        // 每次匹配到当前的范围--Range
        let matchRange = new vscode_1.Range(document.positionAt(match.index), document.positionAt(match.index + match[0].length));
        if (!matchRange.isEmpty)
            // 把Range放入数组
            logStatements.push(matchRange);
    }
    return logStatements;
}
//清楚console.xxx
exports.clearLog = vscode_1.commands.registerCommand("vs-zjplugin.clearLog", () => {
    const editor = vscode_1.window.activeTextEditor;
    if (!editor) {
        return;
    }
    let workspaceEdit = new vscode_1.WorkspaceEdit();
    const document = editor.document;
    const logStatements = getAllLogStatements();
    // 循环遍历每个匹配项的range，并删除
    logStatements.forEach((log) => {
        workspaceEdit.delete(document.uri, log);
    });
    // 完成后显示消息提醒
    vscode_1.workspace.applyEdit(workspaceEdit).then(() => {
        vscode_1.window.showInformationMessage(`${logStatements.length} console.log deleted`);
        vscode_1.commands.executeCommand("editor.action.formatDocument");
    });
});


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("vscode");

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.selectLocalMenu = exports.webview = void 0;
const vscode_1 = __webpack_require__(2);
const Provider_1 = __webpack_require__(4);
const Fs = __webpack_require__(8);
const Open = __webpack_require__(9);
const Path = __webpack_require__(6);
// 提供数据的类
const provider = new Provider_1.default();
// 数据注册
vscode_1.window.registerTreeDataProvider("novel-list", provider);
exports.webview = vscode_1.commands.registerCommand("openSelectedNovel", (args) => {
    // 利用node api拿到文件
    let result = Fs.readFileSync(args.path, "utf-8");
    const panel = vscode_1.window.createWebviewPanel("novelReadWebview", args.name, vscode_1.ViewColumn.One, {
        enableScripts: true,
        retainContextWhenHidden: true,
    });
    // 用pre标签外加部分css使保持原有样式
    panel.webview.html = `<html>
    <body>
      <pre style="font-size: 14px;flex: 1 1 auto;white-space: pre-wrap;word-wrap: break-word;">
        ${result}
      <pre>
    </body>
  </html>`;
});
exports.selectLocalMenu = vscode_1.commands.registerCommand("vs-zjplugin.selectLocalMenu", () => { });


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const NovelTreeItem_1 = __webpack_require__(5);
const getLocalBooks_1 = __webpack_require__(7);
class DataProvider {
    // 提供单行的UI展示
    getTreeItem(info) {
        return new NovelTreeItem_1.default(info);
    }
    // 提供每一行的数据
    getChildren() {
        return getLocalBooks_1.getLocalBooks();
    }
}
exports.default = DataProvider;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = __webpack_require__(2);
const path = __webpack_require__(6);
function getExtname() { }
class NovelTreeItem extends vscode_1.TreeItem {
    constructor(info) {
        super(`${info.name}`);
        console.log(info);
        const extname = path.extname(info.path);
        const tips = [`名称: ${info.name}${extname}`];
        this.info = info;
        // tooltip是悬浮的条提示
        this.tooltip = tips.join("\r\n");
        // 我们设置一下点击该行的命令，并且传参进去
        this.command = {
            command: "openSelectedNovel",
            title: "打开该小说",
            arguments: [{ name: info.name, path: info.path }],
        };
    }
}
exports.default = NovelTreeItem;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.getLocalBooks = void 0;
const vscode_1 = __webpack_require__(2);
const Fs = __webpack_require__(8);
const Path = __webpack_require__(6);
const LocalNovelsPath = "/Users/zhujunwei/Desktop";
function getLocalBooks() {
    let { location } = vscode_1.workspace.getConfiguration("fileDir");
    console.log(location);
    const files = Fs.readdirSync(location || LocalNovelsPath);
    const loaclnovellist = [];
    files.forEach((file) => {
        const extname = Path.extname(file).substr(1);
        if (extname === "txt") {
            const name = Path.basename(file, ".txt");
            const path = Path.join(location || LocalNovelsPath, file);
            loaclnovellist.push({
                path,
                name,
            });
        }
    });
    return Promise.resolve(loaclnovellist);
}
exports.getLocalBooks = getLocalBooks;


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {
const {promisify} = __webpack_require__(10);
const path = __webpack_require__(6);
const childProcess = __webpack_require__(11);
const fs = __webpack_require__(8);
const isWsl = __webpack_require__(12);
const isDocker = __webpack_require__(14);

const pAccess = promisify(fs.access);
const pExecFile = promisify(childProcess.execFile);

// Path to included `xdg-open`.
const localXdgOpenPath = path.join(__dirname, 'xdg-open');

// Convert a path from WSL format to Windows format:
// `/mnt/c/Program Files/Example/MyApp.exe` → `C:\Program Files\Example\MyApp.exe`
const wslToWindowsPath = async path => {
	const {stdout} = await pExecFile('wslpath', ['-w', path]);
	return stdout.trim();
};

// Convert a path from Windows format to WSL format
const windowsToWslPath = async path => {
	const {stdout} = await pExecFile('wslpath', [path]);
	return stdout.trim();
};

// Get an environment variable from Windows
const wslGetWindowsEnvVar = async envVar => {
	const {stdout} = await pExecFile('wslvar', [envVar]);
	return stdout.trim();
};

module.exports = async (target, options) => {
	if (typeof target !== 'string') {
		throw new TypeError('Expected a `target`');
	}

	options = {
		wait: false,
		background: false,
		allowNonzeroExitCode: false,
		...options
	};

	let command;
	let {app} = options;
	let appArguments = [];
	const cliArguments = [];
	const childProcessOptions = {};

	if (Array.isArray(app)) {
		appArguments = app.slice(1);
		app = app[0];
	}

	if (process.platform === 'darwin') {
		command = 'open';

		if (options.wait) {
			cliArguments.push('--wait-apps');
		}

		if (options.background) {
			cliArguments.push('--background');
		}

		if (app) {
			cliArguments.push('-a', app);
		}
	} else if (process.platform === 'win32' || (isWsl && !isDocker())) {
		const windowsRoot = isWsl ? await wslGetWindowsEnvVar('systemroot') : process.env.SYSTEMROOT;
		command = String.raw`${windowsRoot}\System32\WindowsPowerShell\v1.0\powershell${isWsl ? '.exe' : ''}`;
		cliArguments.push(
			'-NoProfile',
			'-NonInteractive',
			'–ExecutionPolicy',
			'Bypass',
			'-EncodedCommand'
		);

		if (isWsl) {
			command = await windowsToWslPath(command);
		} else {
			childProcessOptions.windowsVerbatimArguments = true;
		}

		const encodedArguments = ['Start'];

		if (options.wait) {
			encodedArguments.push('-Wait');
		}

		if (app) {
			if (isWsl && app.startsWith('/mnt/')) {
				const windowsPath = await wslToWindowsPath(app);
				app = windowsPath;
			}

			// Double quote with double quotes to ensure the inner quotes are passed through.
			// Inner quotes are delimited for PowerShell interpretation with backticks.
			encodedArguments.push(`"\`"${app}\`""`, '-ArgumentList');
			appArguments.unshift(target);
		} else {
			encodedArguments.push(`"\`"${target}\`""`);
		}

		if (appArguments.length > 0) {
			appArguments = appArguments.map(arg => `"\`"${arg}\`""`);
			encodedArguments.push(appArguments.join(','));
		}

		// Using Base64-encoded command, accepted by PowerShell, to allow special characters.
		target = Buffer.from(encodedArguments.join(' '), 'utf16le').toString('base64');
	} else {
		if (app) {
			command = app;
		} else {
			// When bundled by Webpack, there's no actual package file path and no local `xdg-open`.
			const isBundled =  false || __dirname === '/';

			// Check if local `xdg-open` exists and is executable.
			let exeLocalXdgOpen = false;
			try {
				await pAccess(localXdgOpenPath, fs.constants.X_OK);
				exeLocalXdgOpen = true;
			} catch (_) {}

			const useSystemXdgOpen = process.versions.electron ||
				process.platform === 'android' || isBundled || !exeLocalXdgOpen;
			command = useSystemXdgOpen ? 'xdg-open' : localXdgOpenPath;
		}

		if (appArguments.length > 0) {
			cliArguments.push(...appArguments);
		}

		if (!options.wait) {
			// `xdg-open` will block the process unless stdio is ignored
			// and it's detached from the parent even if it's unref'd.
			childProcessOptions.stdio = 'ignore';
			childProcessOptions.detached = true;
		}
	}

	cliArguments.push(target);

	if (process.platform === 'darwin' && appArguments.length > 0) {
		cliArguments.push('--args', ...appArguments);
	}

	const subprocess = childProcess.spawn(command, cliArguments, childProcessOptions);

	if (options.wait) {
		return new Promise((resolve, reject) => {
			subprocess.once('error', reject);

			subprocess.once('close', exitCode => {
				if (options.allowNonzeroExitCode && exitCode > 0) {
					reject(new Error(`Exited with code ${exitCode}`));
					return;
				}

				resolve(subprocess);
			});
		});
	}

	subprocess.unref();

	return subprocess;
};

/* WEBPACK VAR INJECTION */}.call(this, "/"))

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("util");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("child_process");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

const os = __webpack_require__(13);
const fs = __webpack_require__(8);
const isDocker = __webpack_require__(14);

const isWsl = () => {
	if (process.platform !== 'linux') {
		return false;
	}

	if (os.release().toLowerCase().includes('microsoft')) {
		if (isDocker()) {
			return false;
		}

		return true;
	}

	try {
		return fs.readFileSync('/proc/version', 'utf8').toLowerCase().includes('microsoft') ?
			!isDocker() : false;
	} catch (_) {
		return false;
	}
};

if (process.env.__IS_WSL_TEST__) {
	module.exports = isWsl;
} else {
	module.exports = isWsl();
}


/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("os");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

const fs = __webpack_require__(8);

let isDocker;

function hasDockerEnv() {
	try {
		fs.statSync('/.dockerenv');
		return true;
	} catch (_) {
		return false;
	}
}

function hasDockerCGroup() {
	try {
		return fs.readFileSync('/proc/self/cgroup', 'utf8').includes('docker');
	} catch (_) {
		return false;
	}
}

module.exports = () => {
	if (isDocker === undefined) {
		isDocker = hasDockerEnv() || hasDockerCGroup();
	}

	return isDocker;
};


/***/ })
/******/ ]);
//# sourceMappingURL=extension.js.map