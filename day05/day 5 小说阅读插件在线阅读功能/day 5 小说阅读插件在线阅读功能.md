## day 5 小说阅读插件在线阅读功能

### 1 WebView基础

昨天我们只是简单的操作了WebView，今天我们先掌握一点点必备的知识

#### 1.1 生命周期

web view 生命周期
生命周期包括三部分：

创建：panel = vscode.window.createWebviewPanel()

显示：panel.webview.html = htmlString

关闭：panel.dispose() 主动关闭，panel.onDidDispose 设置关闭时清理的内容。
```
export function webViewPanel(context: vscode.ExtensionContext) {
  // 1. 使用 createWebviewPanel 创建一个 panel，然后给 panel 放入 html 即可展示 web view
  const panel = vscode.window.createWebviewPanel(
    'helloWorld',
    'Hello world',
    vscode.ViewColumn.One, // web view 显示位置
    {
      enableScripts: true, // 允许 JavaScript
      retainContextWhenHidden: true // 在 hidden 的时候保持不关闭
    }
  );
  const innerHtml = `<h1>Hello Web View</h1>`;
  panel.webview.html = getWebViewContent(innerHtml);

  // 2. 周期性改变 html 中的内容，因为是直接给 webview.html 赋值，所以是刷新整个内容
  function changeWebView() {
    const newData = Math.ceil(Math.random() * 100);
    panel.webview.html = getWebViewContent(`${innerHtml}<p>${newData}</p>`);
  }
  const interval = setInterval(changeWebView, 1000);

  // 3. 可以通过设置 panel.onDidDispose，让 webView 在关闭时执行一些清理工作。
  panel.onDidDispose(
    () => {
      clearInterval(interval);
    },
    null,
    context.subscriptions
  );
}

function getWebViewContent(body: string, pic?: vscode.Uri) {
  return `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
  </head>
  <body>
    ${body}
    <br />
    <img
      id="picture"
      src="${pic || 'https://media.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif'}"
      width="300" />
  </body>
</html>
  `;
}
```
#### 1.2 加载本地资源

昨天我们取巧，利用node api直接读取文件内容放入WebView的html中，类似SSR效果，但是纯前端手法还是把资源包括css、js和图片引入HTML里面，但是这稍有不同

出于安全考虑，WebView默认无法直接访问本地资源，它在一个孤立的上下文中运行，想要加载本地图片、js、css等必须通过特殊的vscode-resource:协议，网页里面所有的静态资源都要转换成这种格式，否则无法被正常加载。

vscode-resource:协议类似于file:协议，但它只允许访问特定的本地文件。和file:一样，vscode-resource:从磁盘加载绝对路径的资源。
默认情况下，vscode-resource:只能访问以下位置中的资源：
1.扩展程序安装目录中的文件。
2.用户当前活动的工作区内。
3.当然，你还可以使用dataURI直接在Webview中嵌入资源，这种方式没有限制；


在高于 1.38 版 VSCode 下可以使用 panel.webview.asWebviewUri(onDiskPath) 生成对应的地址，否则需要使用onDiskPath.with({ scheme: 'vscode-resource' })
```
export function webViewLocalContent(context: vscode.ExtensionContext) {
  const panel = vscode.window.createWebviewPanel(
    'HelloWebViewLocalContent',
    'Web View Local Content',
    vscode.ViewColumn.One,
  );

  const onDiskPath = vscode.Uri.file(
    path.join(context.extensionPath, 'media', 'cat.jpg')
  );
  // 生成一个特殊的 URI 来给 web view 用。
  // 实际是：vscode-resource: 开头的一个 URI
  // 资源文件只能放到插件安装目录或则用户当前工作区里面
  // 1.38以后才有这个 API，前面版本可以用onDiskPath.with({ scheme: 'vscode-resource' });
  const catPicSrc = panel.webview.asWebviewUri(onDiskPath);
  const body = `<h1>hello local cat</h1>`;
  panel.webview.html = getWebViewContent(body, catPicSrc);
}
```
#### 1.3 状态保持
当WebView移动到后台又再次显示时，WebView中的任何状态都将丢失。

解决此问题的最佳方法是使你的WebView无状态，通过消息传递来保存WebView的状态。

**state**
在webview的js中我们可以使用vscode.getState()和vscode.setState()方法来保存和恢复JSON可序列化状态对象。当webview被隐藏时，即使webview内容本身被破坏，这些状态仍然会保存。当然了，当webview被销毁时，状态将被销毁。

**序列化**
通过注册WebviewPanelSerializer可以实现在VScode重启后自动恢复你的webview，当然，序列化其实也是建立在getState和setState之上的。

注册方法：vscode.window.registerWebviewPanelSerializer

**retainContextWhenHidden**
对于具有非常复杂的UI或状态且无法快速保存和恢复的webview，我们可以直接使用retainContextWhenHidden选项。设置retainContextWhenHidden: true后即使webview被隐藏到后台其状态也不会丢失。

尽管retainContextWhenHidden很有吸引力，但它需要很高的内存开销，一般建议在实在没办法的时候才启用。
getState和setState是持久化的首选方式，因为它们的性能开销要比retainContextWhenHidden低得多。


### 2 在线功能添加
![Alt text](./1606915606153.png)

#### 2.1 列表改造

我们先对Provider进行改造，确保本地的列表和在线的列表互相替换，也可以做成本地一组，在线一组同时显示在列表上。这次我们实现第一种

```
import { window, Event, EventEmitter, TreeDataProvider } from 'vscode'
import { getChapter, getLocalBooks } from './utils'
import NovelTreeItem from './NovelTreeItem'
import OnlineTreeItem from './OnlineTreeItem'

export default class DataProvider implements TreeDataProvider<Novel> {

	// 发布订阅的事件
    public refreshEvent: EventEmitter<Novel | null> = new EventEmitter<Novel | null>()
	// 挂到该方法上实现刷新
    onDidChangeTreeData: Event<Novel | null> = this.refreshEvent.event

    // 判断列表是本地还是在线
    public isOnline = false;

    public treeNode: Novel[] = [];

    constructor() {
	    // 默认列表上先加载本地
        getLocalBooks().then((res) => {
            this.treeNode = res;
        })
    }
	// 封装一个本地和在线的切换方法
    refresh(isOnline: boolean) {
        this.isOnline = isOnline;
        this.refreshEvent.fire(null)
    }
	// 根据本地还是在线会加载不同的列表项
    getTreeItem(info: Novel): NovelTreeItem {
        
        if (this.isOnline) return new OnlineTreeItem(info);

        return new NovelTreeItem(info)
    }
	// 现在把列表每项的数据放在treenode上，除了在线小说展开章节的情况
    async getChildren(element?: Novel | undefined): Promise<Novel[]> {
        console.log('element',element);
        if (element) {
            return await getChapter(element.path);
        }
        return this.treeNode
    }

```


TreeDataProvider 需要提供一个 onDidChangeTreeData 属性，该属性是 EventEmitter 的一个实例，然后通过触发 EventEmitter 实例进行数据的更新，每次调用 refresh 方法相当于重新调用了 getChildren 方法。


下面实现一下OnlineTreeItem
```
import { TreeItem, TreeItemCollapsibleState } from 'vscode'

export default class OnlineTreeItem extends TreeItem {

    constructor(info: Novel) {
        super(`${info.name}`)

        const tips = [
            `名称:　${info.name}`,
        ]

        this.tooltip = tips.join('\r\n');
		// 根据isDirectory属性判断是不是可折叠的组
        this.collapsibleState = info.isDirectory ? TreeItemCollapsibleState.Collapsed : TreeItemCollapsibleState.None;
		// 这里命令也换了一个，换成openOnlineNovel（注意注册一下）
        this.command = info.isDirectory ? undefined : {
            command: "openOnlineNovel",
            title: "打开该网络小说",
            arguments: [{ name: info.name, path: info.path }]
        } 
    }

    contextValue = 'online';
}
```

#### 2.2 添加搜索按钮
我们在 列表栏左上角 先加一个 ‘在线搜索’ 命令  searchOnlineNovel，（别忘了要提前在contributes里注册）

```
		"menus": {
			// ...
			"view/title": [
// ... 
				{
					"command": "searchOnlineNovel",
					"when": "view == novel-list",
					"group": "navigation"
				}
			]
	    }
```
下一步，我们把在线搜索方法searchOnline 关联到这个command
```
commands.registerCommand('searchOnlineNovel', () => searchOnline(provider)),
```

然后，执行searchOnline 时，我们弹出对话框，引导用户输入要搜索的小说：

```
export const searchOnline = async function (provider: DataProvider) {
        const msg = await window.showInputBox({
            password: false,
            ignoreFocusOut: false,
            placeHolder: '请输入小说的名字',
            prompt: ''
        });
        if (msg) {
            provider.treeNode = await search(msg);
            provider.refresh(true);
        }
};

```
我们在search方法里面发送请求，更新列表数据 ⤵️

#### 2.2 封装发请求方法

我们先封装一个基于http模块发请求的promise化方法，或者大家直接用axios方法
```
import * as https from 'https';
const request = async (url: string): Promise<string> => {
    return new Promise((resolve, reject) => {
        https.get(url, (res) => {
            let chunks = ''
            if (!res || res.statusCode !== 200) {
                reject(new Error('网络请求错误!'))
                return
            }
            res.on('data', (chunk) => {
                chunks += chunk.toString('utf8')
            })
            res.on('end', () => {
                resolve(chunks)
            })
        })
    })
}
```

#### 2.3 搜小说api

爬取小说站内容不是我们教程的重点和核心，在这里可以不用理解，直接复制粘贴即可

我们需提前安装一下cheerio这个包，一个像JQuery一样操作Html页面内容的包，有更好的也可以替代掉

```
import * as cheerio from 'cheerio';

// 我们从笔趣阁小说站抓页面
const DOMAIN = 'https://www.biquge.com.cn'; 

// 搜索关键词相对应的小说
export const search = async (keyword: string)=>{
    const result = [] as any;
    try {
        const res = await request(DOMAIN + '/search.php?q=' + encodeURI(keyword));
        console.log(res);

        const $ = cheerio.load(res);
        $('.result-list .result-item.result-game-item').each(function (i: number, elem: any) {
            const title = $(elem).find('a.result-game-item-title-link span').text();
            const author = $(elem).find('.result-game-item-info .result-game-item-info-tag:nth-child(1) span:nth-child(2)').text();
            const path = $(elem).find('a.result-game-item-pic-link').attr().href;
            console.log(title, author, path);

            result.push(
                    {
                        type: '.biquge',
                        name: `${title} - ${author}`,
                        isDirectory: true,
                        path
                    }
            );
        });
    } catch(error) {
        console.warn(error);
    }
    return result;
}

// 搜索该小说对应的章节
export const getChapter = async (pathStr: string)=> {
    const result = [] as any;
    try {
        const res = await request(DOMAIN + pathStr);
        const $ = cheerio.load(res);
        $('#list dd').each(function (i: number, elem: any) {
            const name = $(elem).find('a').text();
            const path = $(elem).find('a').attr().href;
            result.push(
                {
                    type: '.biquge',
                    name,
                    isDirectory: false,
                    path
                }
            );
        });
    } catch(error) {
        console.warn(error);
    }
        return result;
}
// 获取章节对应的内容并html化
export const getContent = async(pathStr: string)=> {
    let result = '';
    try {
        const res = await request(DOMAIN + pathStr);
        const $ = cheerio.load(res);
        const html = $('#content').html();
        result = html ? html : '';
    } catch(error) {
        console.warn(error);
    }
    return result;
}

```

#### 2.4 执行命令，打开WebView
这一步没什么好说，之前openOnlineNovel命令已经放在OnlineTreeItem里面了，点击就会执行它
```
		commands.registerCommand(
			'openOnlineNovel',
			async function (args) {
				const panel = window.createWebviewPanel(
					'novelReadWebview', 
					args.name,
					ViewColumn.One, 
					{
						enableScripts: true, 
						retainContextWhenHidden: true, 
					}
				);
				// 把获取的小说内容的html 直接放到webview中
				panel.webview.html =  await getContent(args.path);

			}
		)

```

### 3 小结&任务
引用他人的一段话，在使用webview之前，请作以下考虑：

这个功能真的需要VS Code来提供吗？分离成一个应用或者网站会不会更好？
webview是实现这个特性的最后方案吗？VS Code原生API是否能达到同样的目的呢？
你的webview所牺牲的高资源占用是否能换得同样的用户价值？
请记住：不要因为能使用webview而滥用webview。

**留的任务：**

1.自定义一份css文件，并导入WebView使用，使界面更加美观

2.在线搜索时在过程中和结束后给出提示

3.（稍难）底部状态栏 阅读进度显示
![Alt text](./1606894426871.png)






