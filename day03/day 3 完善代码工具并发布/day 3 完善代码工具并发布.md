## day 3 完善代码工具并发布
> 实现功能
>  1. 一键删除当前页面所有console 
>  2. 一键删除当前页面所有注释并格式化代码

### 1 一键删除所有console
我们首先获取所有log位置，然后把该位置置成空
#### 1.1 获取所有console
```
function getAllLogStatements() {
	const editor = vscode.window.activeTextEditor;
	// 获取编辑器页面文本
	const document = editor.document;
	const documentText = document.getText();

	let logStatements = [];
	// 检测console的正则表达式
	const logRegex = /console.(log|debug|info|warn|error|assert|dir|dirxml|trace|group|groupEnd|time|timeEnd|profile|profileEnd|count)\((.*)\);?/g;
	let match;
	// 正则循环匹配页面文本
	while (match = logRegex.exec(documentText)) {
	// 每次匹配到当前的范围--Range
		let matchRange =
			new vscode.Range(
				document.positionAt(match.index),
				document.positionAt(match.index + match[0].length)
			);
		if (!matchRange.isEmpty)
		// 把Range放入数组
			logStatements.push(matchRange);
	}
	return logStatements;
}
```

#### 1.2 把获取到的console逐个删除
```
	const deleteAllLog = vscode.commands.registerCommand('firstExt.delLog', () => {
		const editor = vscode.window.activeTextEditor;
		if (!editor) { return; }

		let workspaceEdit = new vscode.WorkspaceEdit();
		const document = editor.document;

		const logStatements = getAllLogStatements();

		// 循环遍历每个匹配项的range，并删除
		logStatements.forEach((log) => {
			workspaceEdit.delete(document.uri, log);
		});
		// 完成后显示消息提醒
		vscode.workspace.applyEdit(workspaceEdit).then(() => {
			vscode.window.showInformationMessage(`${logStatements.length} console.log deleted`);
		});
	});
	context.subscriptions.push(deleteAllLog);
```
- vscode.WorkspaceEdit 工作区编辑方法，可操作多个文件，也可新增或删除 文本、文件、文件夹
#### 
### 2 一键删除所有注释并格式化代码
第一个思路还是正则替换法
```
	let removeComments = vscode.commands.registerCommand('firstExt.removeComments', function () {
		const editor = vscode.window.activeTextEditor;
		editor.edit(editBuilder => {
			let text = editor.document.getText();
			// 正则匹配替换掉注释文本
			text = text.replace(/((\/\*([\w\W]+?)\*\/)|(\/\/(.(?!"\)))+)|(^\s*(?=\r?$)\n))/gm, '').replace(/(^\s*(?=\r?$)\n)/gm, '').replace(/\\n\\n\?/gm, '');
			// 全量替换当前页面文本
			const end = new vscode.Position(editor.document.lineCount + 1, 0);
			editBuilder.replace(new vscode.Range(new vscode.Position(0, 0), end), text);
			// 执行格式化代码操作
			vscode.commands.executeCommand('editor.action.formatDocument');
		});
	});

	context.subscriptions.push(removeComments);
```
- 调用VSCode内部命令方法
vscode.commands.executeCommand 传入参数是内置api名称
![Alt text](./1602857764728.png)
VSCode暴露了大量的自身api可供选择，善用api完全可以打造一套VSCode工作流

### 3 打包 & 发布

#### 3.1 打包
全局安装`vsce npm install vsce -g`(打包工具)
在插件目录下运行 `vsce package`打包
完成后多出一个.vsix的文件，打包成功

PS：如果不想发布到应用市场，直接把.vsix的文件发给别人，拖拽到VSCode左边栏即可安装（但插件无法自动升级）

#### 3.2 发布到应用市场
需要提前注册账号，并保存好token，请查看官方文档
https://code.visualstudio.com/api/working-with-extensions/publishing-extension


#### 4 小结&任务
完成了一个较完善的代码工具

**留的任务**

1.目前是一个较完整的代码插件了，请大胆的发布它并推广

2.以上代码还有少数bug：
① insertLog插件需要排除文本中的单双引号，不然会出错
② http的url会被正则错误匹配为//，请排除
其他等等
请修改后一键增量发布到市场

3.这三个功能的实现有很多不一样的思路，条条大路通罗马，请活用编辑器的insert、delete和replace还有官方内置api来重新实现一下