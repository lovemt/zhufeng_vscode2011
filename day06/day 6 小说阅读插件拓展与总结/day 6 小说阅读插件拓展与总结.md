## day 6 小说阅读插件拓展与总结

> 之前讲的WebView基础还剩下一部分重要的知识：WebView和插件本身通信，这次通过实战来学会吧

### 1 WebView中JS与插件通信

#### 1.1 将插件的信息传递到WebView
插件可以用webview.postMessage()将数据发送到它的WebView中。这个方法能发送任何序列化的JSON数据到WebView中，在 WebView 中通过window.addEventListener('message', callback)监听message信息。

#### 1.2 将WebView的信息传递到插件中
WebView也可以把信息传递回对应的插件中，用VSCode API 为WebView提供的postMessage函数我们就可以完成这个目标。调用WebView中的acquireVsCodeApi获取VSCode API对象。这个函数在一个会话中只能调用一次，你必须保持住这个方法返回的VSCode API实例，然后再转交到需要调用这个实例的地方。

**注意**：WebView的js脚本能做到任何普通网页js脚本能做到的事情，但是WebView运行在自己的上下文中，js脚本不能访问VSCode的API。并且出于安全性考虑，你必须保证VSCode的API的私有性，让它不会泄露到全局状态中。


### 2 实现在线小说添加收藏功能
#### 2.1 设置项
VSCode持久化存储的话，它的Memento api有点麻烦，变通一下，我们可以做成设置项或者利用node写入和读取文件。

我们先把收藏夹栏的收藏图书列表放入设置项中
但是要注意全局与工作区的区别
> VSCode提供了两种不同的设置范围：
用户设置-全局应用于您打开的任何VSCode实例的设置。
工作区设置-存储在工作区中的设置，仅在打开工作区时适用。
注意：工作区设置将覆盖用户设置。工作区设置特定于项目，并且可以在项目的开发人员之间共享。
```
		// package.json中
		"configuration": {
			"type": "object",
			"title": "小说插件配置", // 会显示在用户设置标题上
			"properties": {
				"novel.favorites": { // 用户设置具体项
					"type": "array",
					"default": [],
					"scope": "window", // 范围是全局
					"description": "小说收藏名单"
				}
			}
		}
```

正确添加后效果如图：
![Alt text](./1607162842010.png)

#### 2.2 添加按钮
实现效果：
![Alt text](./1607162160543.png)

我们先找到它的位置，如下图：
![Alt text](./1606889993078.png)

先加一个command ：addFavorite，这个不多说了

然后我们找到上图的位置，添加列表右侧命令，注意when条件是要在novel-list栏并且是在线小说
![Alt text](./1607162552087.png)

同样的，我们在NovelTreeItem和OnlineTreeItem中的contextValue也要做区分
![Alt text](./1607162730585.png)
![Alt text](./1607162745310.png)

如此，我们就实现了添加按钮的功能
#### 2.3 侧边栏加入收藏列表
![Alt text](./1607162904200.png)

我们现在view配置中添加一项 收藏栏
![Alt text](./1607162991924.png)

而后改造整体逻辑
我们要在extension.ts中重新注册一个数据提供者Provider
![Alt text](./1607163254566.png)
```
import { window, Event, EventEmitter, TreeDataProvider, workspace } from 'vscode'
import { getChapter, getLocalBooks } from './utils'
import NovelTreeItem from './NovelTreeItem'
import OnlineTreeItem from './OnlineTreeItem'
export default class FavoriteProvider implements TreeDataProvider<Novel> {

    public refreshEvent: EventEmitter<Novel | null> = new EventEmitter<Novel | null>()

    readonly onDidChangeTreeData: Event<Novel | null> = this.refreshEvent.event

    refresh() {
        this.refreshEvent.fire(null)
    }

    getTreeItem(info: Novel): NovelTreeItem {
    // ui可以复用OnlineTreeItem
        return new OnlineTreeItem(info);
    }

    async getChildren(element?: Novel | undefined): Promise<Novel[]> {
        if (element) {
            return await getChapter(element.path);
        }
        // 收藏栏数据从配置项中拿取
        return workspace.getConfiguration().get('novel.favorites', []);
    }

    contextValue = 'favorite';

}

```

#### 2.3 添加addFavorite命令
```
		// 行上的数据项会自动传递到函数的args中
		commands.registerCommand(`addFavorite`, function(args){
			const config = workspace.getConfiguration();
			let favorites: Novel[] = config.get('novel.favorites', []);
			// 用配置项合并当前项
			favorites = [...favorites,args];
			// 更新配置后触发收藏栏刷新
			// 第三项是true为用户全局设置,反之为当前workspace设置,当前设置会覆盖全局设置,切记!
			config.update('novel.favorites', favorites, true).then(() => {
				favoriteProvider.refresh();
			})
		}),
```
### 3 记录本地小说阅读进度
我们还是用设置项来应付一下持久化存储

#### 3.1 报告进度
报告进度的话是WebView隔一段时间向插件报告进度，插件接收
```
// webview滚动条报告给插件 要在panel.webview.html中加入

..... </body> 之前
<script>
    const vscode = acquireVsCodeApi();
    setInterval(() => {
        vscode.postMessage({
            command: 'updateProgress',
            progress: window.scrollY / document.body.scrollHeight
        })
    }, 1000) // 定时每一秒触发一次
</script>
```
```
// openSelectedNovel 命令中 接收处理webview中报告的信息
panel.webview.onDidReceiveMessage(handleMessage, undefined, context.subscriptions);
```
#### 3.2 存储进度到全局设置 handleMessage
```
handleMessage  = (message) => {
		const progressSetting = workspace.getConfiguration().get('novel.progress', {} as any);
		progressSetting[args.name] = message.progress;
		switch (message.command) {
			case 'updateProgress':
				return workspace.getConfiguration().update('novel.progress', progressSetting, true);
					}
				}
```
#### 3.3 读取进度
读取进度的话是WebView初始化后，插件向WebView发消息，告诉进度了，WebView接收后滚动到对应位置
```
// 插件向WebView发消息，告诉记录中的进度
const defaultProgess = {} as any;
defaultProgess[args.name] = 0;
panel.webview.postMessage({
	command: 'goProgress',
	progress: workspace.getConfiguration().get('novel.progress', defaultProgess)[args.name]
});
```

```
..... </body> 之前
<script>
    // webview收插件消息并滚动
    window.addEventListener('message', event => {
        const message = event.data; // The JSON data our extension sent
        switch (message.command) {
            case 'goProgress':
                window.scrollTo(0, document.body.scrollHeight * message.progress);
                break;
        }
    });
</script>
```
这样我们每次看一部分本地小说，再次打开时就回到原来阅读的进度

### 4 本地小说css美化和外置js导入

我们还是对本地小说的WebView下手，在openSelectedNovel命令下创建的novelReadWebview，我们更改其html如下（添加了link和script标签）：
```
panel.webview.html = `<!DOCTYPE html>
<html>
	<head>
		
        <link rel="stylesheet" href="${cssSrc}">
    </head>
	<body>
		<div class="content">
			<pre style="flex: 1 1 auto;white-space: pre-wrap;word-wrap: break-word;">
				${result}
			<pre>
		</div>
	</body>
	
	<script src="${jsSrc}">
    </script>
</html>`
```
jsSrc和cssSrc地址的话我们用到昨天的知识，利用asWebviewUri这个api转化地址


```
const jsSrc = panel.webview.asWebviewUri(Uri.file(
	Path.join(context.extensionPath, 'static', 'localnovel.js')
));
const cssSrc = panel.webview.asWebviewUri(Uri.file(
	Path.join(context.extensionPath, 'static', 'localnovel.css')
));
```

我们新建static文件夹，放入相应的js和css文件
![Alt text](./1607161840542.png)

把原scripts标签里面内容挪入js

![Alt text](./1607161956207.png)

css文件中对照调试工具里面的节点层级、类名id等改变一些样式（大家可以让页面变的更美观一些）

![Alt text](./1607162023171.png)

### 完结说明

实现到目前是一个比较可用的小说插件了，目前大部分的插件开发要点应该都实践过了，这也意味着本次训练营教程就到这里了，相信大家还有些意犹未尽。以后小说插件也可以不停的完善，比如实现追更新功能、换小说站源、本地小说分章节以及各种可选样式主题等等。

最后，感谢张佬和艳旭的支持信任、大家的耐心阅读，后期反馈和问题都可以在群里问，我会积极解答！

**留的任务**
1. 网络在线小说 键盘⬅️➡️切换章节功能和⤴️⤵️键滚动整个屏幕内容功能

#### 训练营参考过的资料文档

**一些官网资料**

VSCode WebView说明
https://code.visualstudio.com/api/extension-guides/webview

VSCode 自身内置命令大全
https://code.visualstudio.com/docs/getstarted/keybindings


**参考文档**：

参考过的一些不错的博客和类库

小茗同学的博客园 https://www.cnblogs.com/liuxianan/p/vscode-plugin-webview.html

VSCode插件开发入门 https://zhuanlan.zhihu.com/p/99198980

从零开始实现VS Code基金插件 https://juejin.cn/post/6864712731484749831

github小说插件原型 https://github.com/aooiuu/z-reader

