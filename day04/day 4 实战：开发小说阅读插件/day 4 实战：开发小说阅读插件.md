
## day 4 实战：开发小说阅读插件
> 这次我们利用插件中提供的WebView功能着手实现vscode中的看小说插件，教程完成后，大家都可以轻松的用内置webview实现各种功能了（炒股、听音乐、小游戏等😀）

今天我们先实现一个本地的小说插件

今天的效果图：
![Alt text](./1607078388267.png)

### 1 添加侧边栏
#### 1.1 图标栏

我们首先在我们的应用中创建一个视图容器，视图容器简单来说一个单独的侧边栏，在 package.json 的 contributes.viewsContainers 中进行配置。

（icon请自寻一枚svg图标）
```
{
  "contributes": {
    "viewsContainers": {
      "activitybar": [
        {
          "id": "novel-read",
          "title": "NOVEL READ",
          "icon": "images/pdf.svg"
        }
      ]
    }
  }
}
```
#### 1.2 view树

我们需要在view树中添加一个视图，在 package.json 的 contributes.views 中进行配置，该字段为一个对象，它的 Key 就是我们视图容器的 id，值为一个数组，表示一个视图容器内可添加多个视图。
```
{
  "contributes": {
    .........以上是图标配置
    "views": {
			"novel-read": [
				{
					"name": "本地列表栏",
					"id": "novel-list"
				}，
				
			]
    }
  }
}

```
顺便我们配置一下对应的激活事件：
```
{
	"activationEvents": [
    // 表示点击图标激活插件时，展示本地小说列表栏
		"onView:novel-list"
	]
}
```
我们需要通过 vscode 提供的 registerTreeDataProvider 为视图提供数据。打开生成的 src/extension.ts 文件，修改代码如下：

```
import { ExtensionContext, commands, window, workspace } from 'vscode';
import Provider from './Provider';

// 激活插件
export function activate(context: ExtensionContext) {
  // 提供数据的类
  const provider = new Provider();

  // 数据注册
  window.registerTreeDataProvider('novel-list', provider);
}

export function deactivate() {}

```
这里我们通过 VS Code 提供的 window.registerTreeDataProvider 来注册数据，传入的第一个参数表示列表视图 ID，第二个参数是 TreeDataProvider 的实现。



#### 1.3 实现TreeDataProvider，为view树提供数据

TreeDataProvider 顾名思义，提供数据的，有两个必须实现的方法：

getChildren：我们要拿到每一行的数据供getTreeItem消费，所以返回一个数组；
getTreeItem：该方法接受单个数据，返回视图单行的 UI定义

```

export default class DataProvider implements TreeDataProvider<any> {

	// 提供单行的UI展示
    getTreeItem(info: Novel): NovelTreeItem {
        return new NovelTreeItem(info)
    }
	// 提供每一行的数据
    getChildren(): Promise<Novel[]> {
        return getLocalBooks()
    }
}

```
#### 1.4 实现NovelTreeItem和getLocalBooks 方法
可注册一个全局属性 LocalNovelsPath = “/Users/xxx” （各位可自行定义，是放置文件夹目录的绝对路径）;

然后我们向该目录中放入点txt小说；（我们暂时先加载本地小说，格式暂时只支持txt）

接着我们在getLocalBooks方法中取出目录中的小说名字和路径给TreeItem
```
export function getLocalBooks(): Promise <Novel[]> {

    const files = Fs.readdirSync(LocalNovelsPath);
    const loaclnovellist = [] as any;
    files.forEach((file: string) => {
        const extname = Path.extname(file).substr(1);
        if (extname === 'txt') {
            const name = Path.basename(file, '.txt');
            const path = Path.join(LocalNovelsPath, file);
            loaclnovellist.push({
                path,
                name,
            })
        }
    })
    return Promise.resolve(loaclnovellist)
}
```
我们继承TreeItem，并用super把每一行的标签名确定
```
import { TreeItem } from 'vscode'

export default class NovelTreeItem extends TreeItem {

    constructor(info: Novel) {
        super(`${info.name}`)

        const tips = [
            `名称:　${info.name}`,
        ]

        // tooltip是悬浮的条提示
        this.tooltip = tips.join('\r\n');
        // 我们设置一下点击该行的命令，并且传参进去
        this.command = {
            command: "openSelectedNovel",
            title: "打开该小说",
            arguments: [{ name：info.name,path:info.path }]
        } 
    }
}
```
我们添加command，并直接打开文件
```
	"contributes": {
		"commands": [
			{
				"command": "openSelectedNovel",
				"title": "打开该小说"
			}
		]
	}

--- 在extension.ts中

		vscode.commands.registerCommand('openSelectedNovel', (args) => {
			vscode.commands.executeCommand('vscode.open', vscode.Uri.parse(args.path));
		})
```

如此我们就暂时能用本身自带功能看小说了

### 2 初步WebView界面开发

虽然前面做的差不多，但是还是利用本身自带解析txt功能而不是WebView绘制界面，虽然我们可以做txt文件的解析插件会更快更简单，但这样违背了教程的本意

插件市场上也有很多解析txt小说的插件，感兴趣也可以自己实现一下，不再赘述


#### 2.1 创建WebView
```
context.subscriptions.push(vscode.commands.registerCommand(
	'openSelectedNovel', 
	function (uri) {
	// 创建webview
    const panel = vscode.window.createWebviewPanel(
        'testWebview', // viewType
        "WebView演示", // 视图标题
        vscode.ViewColumn.One, // 显示在编辑器的哪个部位
        {
            enableScripts: true, // 启用JS，默认禁用
            retainContextWhenHidden: true, // webview被隐藏时保持状态，避免被重置
        }
    );
    panel.webview.html = `<html><body>你好，我是Webview</body></html>`
    }
)
  
```
默认情况下当WebView被隐藏时资源会被销毁，通过retainContextWhenHidden: true会一直保存，但会占用较大内存开销，仅在需要时开启；

#### 2.2 简单界面绘制

```
commands.registerCommand(
			'openSelectedNovel',
			function (args) {
		
				// 利用node api拿到文件
				let result = Fs.readFileSync(args.path, 'utf-8')

				const panel = window.createWebviewPanel(
					'novelReadWebview',
					args.name,
					ViewColumn.One,
					{
						enableScripts: true, 
						retainContextWhenHidden: true,
					}
				);
				
				// 用pre标签外加部分css使保持原有样式
				panel.webview.html = `<html>
					<body>
						<pre style="flex: 1 1 auto;white-space: pre-wrap;word-wrap: break-word;">
							${result}
						<pre>
					</body>
				</html>`
			}
		)
```


#### 2.3 检查和调试 WebView
可以使用下面 WebView develop tool 命令
使用快捷键cmd + shift + p 输入webview
![Alt text](./1606723860651.png)

Webview的Content 位于Webview页签内的iframe中，我们可以使用chrome开发工具检查和修改WebView的DOM，并调试WebView页签中运行的js脚本

![Alt text](./1606723873839.png)



### 3 小结&任务

WebView官方文档 https://code.visualstudio.com/api/extension-guides/webview

今天做了一个本地阅读插件，内容主要是围绕侧边栏做处理，WebView介绍的不多，明天会主要设置WebView

从上可以看得出，WebView其实比较慢和消耗性能，大家使用还是要注意场景。

留的任务

1.请在插件设置setting中加入一个用户自定义设置：使作为使用插件的用户可以手动设置 book 目录
![Alt text](./1606823464763.png)

2.列表左上角 加一个快速打开本地图书目录的按钮（提示：可能需要安装‘open’这个npm包）
![Alt text](./1606823316728.png)
![Alt text](./1606823353855.png)

3.（稍难）随时记录本地阅读进度，每次打开该小说回到该位置

